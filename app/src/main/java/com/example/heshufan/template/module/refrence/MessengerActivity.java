package com.example.heshufan.template.module.refrence;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Button;

import com.example.common.util.LogUtil;
import com.example.common.view.MyClockView;
import com.example.heshufan.template.R;
import com.example.heshufan.template.constant.MsgConstants;
import com.example.heshufan.template.service.MessengerService;
import com.example.heshufan.template.view.GuestureView;

import butterknife.ButterKnife;

/**
 * 利用Messenger进行跨进程通信
 * @author heshufan
 */
public class MessengerActivity extends BaseActivity {
    private static final String TAG = "MessengerActivity:";


    //向服务器发送消息的Messenger
    private Messenger mMesseger;

    //处理服务端传递过来的消息
    private Messenger mMessengerFormSer;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        /**
         *
         * @param name
         * @param service 远程service返回的Ibinder
         */
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mMesseger = new Messenger(service);

            //将数据发送给服务端
            Message message = Message.obtain(null, MsgConstants.MSG_FROM_CLIENR);
            Bundle data = new Bundle();
            data.putString("msg", "a msg from client");

            //将客户端的Messenger传递给服务端
            message.replyTo = mMessengerFormSer;
            message.setData(data);
            try {
                mMesseger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    //处理服务端传递过来的消息
    private class MessengerHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MsgConstants.MSG_FROM_SERVICE:
                    LogUtil.d(TAG + msg.getData().getString("msg"));
                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);
        ButterKnife.bind(this);

        //绑定远程service
        Intent mService = new Intent(this, MessengerService.class);
        bindService(mService, serviceConnection, Context.BIND_AUTO_CREATE);

        mMessengerFormSer = new Messenger(new MessengerHandler());
    }

    @Override
    protected void onDestroy() {
        unbindService(serviceConnection);
        super.onDestroy();
    }
}
