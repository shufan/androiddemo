package com.example.heshufan.template.module.refrence;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.common.util.LogUtil;
import com.example.common.view.MyClockView;
import com.example.heshufan.template.R;
import com.example.heshufan.template.view.CircleView;
import com.example.heshufan.template.view.GuestureView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author heshufan
 *         自定义View
 */
public class ViewActivity extends AppCompatActivity implements View.OnClickListener {

    private MyClockView mXiaomiClock;
    private Button doubleTap;
    private GuestureView mGuestureView;
    private CircleView mCircleView;
    private ImageView mWechatBtn;

    @BindView(R.id.isState)
    public Button mState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        ButterKnife.bind(this);

        mXiaomiClock = findViewById(R.id.xiaomiClock);
        doubleTap = findViewById(R.id.doubleTap);
        mGuestureView = findViewById(R.id.guestureView);
        mCircleView = findViewById(R.id.circleView);
        mWechatBtn = findViewById(R.id.wechatBtn);

        mWechatBtn.setOnClickListener(this);
        mState.setOnClickListener(this);


        //自定义view的点击事件
        mCircleView.setOnClickListener(this);

        //自定义点击监听器
        mCircleView.setOnSelfClickListener(new CircleView.OnSelfViewClickListener() {
            @Override
            public void onClick() {
                LogUtil.d("setOnSelfClickListener");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.isState:
                Toast.makeText(this, "点击", Toast.LENGTH_SHORT).show();
                break;
            case R.id.circleView:
                Toast.makeText(this, "点击CircleView", Toast.LENGTH_SHORT).show();
                break;
            case R.id.wechatBtn:
                Intent intent = new Intent(ViewActivity.this,ImageActivity.class);
                int[] location = new int[2];
                mWechatBtn.getLocationOnScreen(location);
                intent.putExtra("x",location[0]);
                intent.putExtra("y",location[1]);
                intent.putExtra("w",mWechatBtn.getWidth());
                intent.putExtra("h",mWechatBtn.getHeight());
                startActivity(intent);

                //R.anim.fade_in后面activity进去的动画
                //R.anim.fade_out前面activity退出的动画
                overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                break;
        }
    }
}
