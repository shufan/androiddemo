package com.example.heshufan.template.app;

import android.app.Application;

import com.example.common.app.Template;
import com.squareup.leakcanary.LeakCanary;

/**
 * describe:自定义Application
 */
public class TemplateApp extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        Template.init(this);
        initLeakCanary();
    }


    /**
     * 内存泄漏检测
     */
    private void initLeakCanary(){
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
    }
}
