package com.example.heshufan.template.module.refrence;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.heshufan.template.R;
import com.example.heshufan.template.module.refrence.mvc.controller.MvcActivity;
import com.example.heshufan.template.module.refrence.mvp.view.MvpActivity;
import com.example.heshufan.template.module.refrence.net.RetorfitAndOkHttp.NetActivity;
import com.example.heshufan.template.module.refrence.net.RxJava2.RxJava2Activity;

/**
 * @author heshufan
 * @date 2019/2/20
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        switch (i) {
            case R.id.one:
                Intent intent1 = new Intent(this, HandlerActivity.class);
                startActivity(intent1);
                break;
            case R.id.two:
                Intent intent2 = new Intent(this, AnimationActivity.class);
                startActivity(intent2);
                break;
            case R.id.three:
                Intent intent3 = new Intent(this, BitmapActivity.class);
                startActivity(intent3);
                break;
            case R.id.four:
                Intent intent4 = new Intent(this, ViewActivity.class);
                startActivity(intent4);
                break;
            case R.id.five:
                Intent intent5 = new Intent(this, PhoteWallActivity.class);
                startActivity(intent5);
                break;
            case R.id.six:
                Intent intent6 = new Intent(this, NetActivity.class);
                startActivity(intent6);
                break;
            case R.id.seven:
                Intent intent7 = new Intent(this, RxJava2Activity.class);
                startActivity(intent7);
                break;
            case R.id.eight:
                Intent intent8 = new Intent(this, MvcActivity.class);
                startActivity(intent8);
                break;
            case R.id.nine:
                Intent intent9 = new Intent(this, MvpActivity.class);
                startActivity(intent9);
                break;
            case R.id.ten:
                Intent intent10 = new Intent(this, MessengerActivity.class);
                startActivity(intent10);
                break;
            case R.id.eleven:
                Intent intent11 = new Intent(this, AIDLActivity.class);
                startActivity(intent11);
                break;
            case R.id.twelve:
                Intent intent12 = new Intent(this, BinderPoolActivity.class);
                startActivity(intent12);
                break;
            default:
                break;
        }
        return true;
    }
}
