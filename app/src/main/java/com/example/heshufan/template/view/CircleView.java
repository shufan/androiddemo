package com.example.heshufan.template.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * 自定义view ，padding处理，点击事件处理
 * @author heshufan
 * @date 2019/4/30
 */

public class CircleView extends View {
    private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private int mColor = Color.RED;
    private int defaultSize = 100;

    //给自定义view设置点击事件
    private OnClickListener mOnClickListener;

    //自定义监听器
    private OnSelfViewClickListener mOnViewClickListener;


    public CircleView(Context context) {
        super(context);
        init();
    }

    private void init() {
        mPaint.setColor(mColor);
    }

    public CircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public CircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();

        int width = getWidth() - paddingLeft - paddingRight;
        int height = getHeight() - paddingBottom - paddingTop;
        int radius = Math.min(width, height) / 2;

        canvas.drawCircle(paddingLeft + width / 2, paddingRight + height / 2, radius, mPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(setMeasure(widthMeasureSpec), setMeasure(heightMeasureSpec));
    }

    private int setMeasure(int spec) {
        int mode = MeasureSpec.getMode(spec);
        int size = MeasureSpec.getSize(spec);
        if (mode == MeasureSpec.EXACTLY) {
            return size;
        } else if (mode == MeasureSpec.AT_MOST) {
            return Math.min(size, defaultSize);
        } else {
            return defaultSize;
        }
    }



    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_UP:
                //点击事件处理在ACTION_UP生效
                //mOnClickListener.onClick(this);
                mOnViewClickListener.onClick();
                break;
        }
        return true;

    }

    public void setOnSelfClickListener(OnSelfViewClickListener listener ){
        this.mOnViewClickListener = listener;
    }

    /**
     * 使用系统的监听器
     * @param onClickListener OnClickListener
     */
    @Override
    public void setOnClickListener(OnClickListener onClickListener) {
        this.mOnClickListener = onClickListener;
    }

    /**
     * 自定义点击事件监听器
     */
    public interface OnSelfViewClickListener {
        void onClick();
    }
}
