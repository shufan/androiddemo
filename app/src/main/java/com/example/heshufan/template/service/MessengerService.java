package com.example.heshufan.template.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import com.example.common.util.LogUtil;
import com.example.heshufan.template.constant.MsgConstants;

/**
 * @author heshufan
 * @date 2019/4/11
 */

public class MessengerService extends Service {

    //方便打印日志
    public static final String TAG = "MessengerService:";

    private Messenger mMessenger = new Messenger(new MessendgerHandler());

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        //将IBinder返回给客户端
        return mMessenger.getBinder();
    }

    /**
     * 处理服务端传递过来的消息
     */
    private static class MessendgerHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MsgConstants.MSG_FROM_CLIENR:
                    //收到客户端的消息
                    LogUtil.d(TAG + msg.getData().getString("msg"));

                    //向客户端传递消息
                    Messenger client = msg.replyTo;
                    Message message = Message.obtain(null, MsgConstants.MSG_FROM_SERVICE);
                    Bundle data = new Bundle();
                    data.putString("msg", "I have reveiced your msg");
                    message.setData(data);
                    try {
                        client.send(message);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                default:
                    super.handleMessage(msg);
            }

        }
    }
}
