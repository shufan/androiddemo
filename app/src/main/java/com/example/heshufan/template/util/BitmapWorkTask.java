package com.example.heshufan.template.util;

/**
 * @author heshufan
 * @date 2019/2/26
 */

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.LruCache;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.common.util.CacheUtil;
import com.example.common.util.LogUtil;
import com.example.common.util.StringUtil;
import com.jakewharton.disklrucache.DiskLruCache;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;

/**
 * 异步线程去加载图片
 * String:传入参数的类型
 */
public class BitmapWorkTask extends AsyncTask<String, Void, Bitmap> {

    String imageUrl;
    private LruCache<String,Bitmap> mLruCache;
    private DiskLruCache mDiskLruCache;
    private Set<BitmapWorkTask> mTasks;
    private GridView mGridView;

    public BitmapWorkTask(LruCache<String, Bitmap> mLruCache, DiskLruCache mDiskLruCache, Set<BitmapWorkTask> mTasks, GridView mGridView) {
        this.mLruCache = mLruCache;
        this.mDiskLruCache = mDiskLruCache;
        this.mTasks = mTasks;
        this.mGridView = mGridView;
    }

    /**
     * 执行耗时操作
     *
     * @param integers 调用execute时传入的参数
     * @return
     */
    @Override
    protected Bitmap doInBackground(String... integers) {
        imageUrl = integers[0];
        FileDescriptor fileDescriptor = null;
        FileInputStream fileInputStream = null;
        DiskLruCache.Snapshot snapShot = null;
        final String key = StringUtil.hashKeyForDisk(imageUrl);

        try {
            snapShot = mDiskLruCache.get(key);
            if (snapShot == null) {
                LogUtil.d("本地磁盘没有相应的图片");
                //缓存没有从网络下载并缓存在本地
                DiskLruCache.Editor editor = mDiskLruCache.edit(key);
                if (editor != null) {
                    OutputStream outputStream = editor.newOutputStream(0);
                    if (downloadUrlToStream(imageUrl, outputStream)) {
                        LogUtil.d("从网络下载图片存在本地");
                        //
                        editor.commit();
                    } else {
                        editor.abort();
                    }
                }
                //图片写入缓存后再次拿到snapshot
                snapShot = mDiskLruCache.get(key);
            }

            //将缓存的数据转换为bitmap
            Bitmap bitmap = null;
            if (snapShot != null) {
                fileInputStream = (FileInputStream) snapShot.getInputStream(0);
                fileDescriptor = fileInputStream.getFD();
            }

            if (fileDescriptor != null) {
                bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            }

            if (bitmap != null) {
                //将图片存入到内存缓存
                LogUtil.d("将下载的图片存入内存缓存");
                CacheUtil.addBitmapToMemoryCache(mLruCache, imageUrl, bitmap);
            }
            return bitmap;

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileDescriptor != null && fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * @param bitmap
     */
    @Override
    protected void onPostExecute(Bitmap bitmap) {
        //通过之前设置的tag找到对应的imageView
        ImageView imageView = mGridView.findViewWithTag(imageUrl);
        if (bitmap != null && imageView != null) {
            imageView.setImageBitmap(bitmap);
        }
        //移除改线程，因为可能会同时创建多个线程去下载图片
        mTasks.remove(this);
    }

    /**
     * 建立HTTP请求，并获取Bitmap对象。
     *
     * @param urlString 图片的URL地址
     * @return 解析后的Bitmap对象
     */
    private boolean downloadUrlToStream(String urlString, OutputStream outputStream) {
        HttpURLConnection urlConnection = null;
        BufferedOutputStream out = null;
        BufferedInputStream in = null;
        try {
            final URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            in = new BufferedInputStream(urlConnection.getInputStream(), 8 * 1024);
            out = new BufferedOutputStream(outputStream, 8 * 1024);
            int b;
            while ((b = in.read()) != -1) {
                out.write(b);
            }
            return true;
        } catch (final IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
