package com.example.heshufan.template.constant;

/**
 * @author heshufan
 * @date 2019/4/11
 */

public class MsgConstants {

    public static final int MSG_FROM_CLIENR = 1;
    public static final int MSG_FROM_SERVICE = 2;
}
