package com.example.heshufan.template.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.common.util.ApkUtil;
import com.example.common.util.CacheUtil;
import com.example.common.util.LogUtil;
import com.example.heshufan.template.R;
import com.example.heshufan.template.util.BitmapWorkTask;
import com.jakewharton.disklrucache.DiskLruCache;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static com.example.common.util.CacheUtil.getBitmapFormCache;

/**
 * @author heshufan
 * @date 2019/2/22
 */

public class PhotoWallAdapter extends ArrayAdapter<String> {

    private LruCache<String, Bitmap> mLurCache;
    private DiskLruCache mDisLruCache;
    private Set<BitmapWorkTask> tasks;
    private Context mContext;
    private GridView mGridView;
    private int mItemHeight = 0;

    public PhotoWallAdapter(Context context, int textViewResourceId, String[] objects,
                            GridView photoWall) {
        super(context, textViewResourceId, objects);
        mContext = context;
        init();
        mGridView = photoWall;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final String url = getItem(position);
        View view;
        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.gridview_item, null);
        } else {
            view = convertView;
        }
        final ImageView imageView = view.findViewById(R.id.gridviewItemIV);
        if (imageView.getLayoutParams().height != mItemHeight) {
            imageView.getLayoutParams().height = mItemHeight;
        }
        imageView.setImageResource(R.drawable.image_place_holder);
        //todo 给ImageView设置一个Tag，保证异步加载图片时不会乱序
        imageView.setTag(url);
        loadBitmap(imageView, url);
        return view;

    }

    /**
     * 加载图片:先去本地磁盘查找图片缓存，不存在就去网络下载，下载后保存在本地磁盘中，然后转化为bitmap后，
     * 然后在保存在内存缓存中
     *
     * @param imageView
     * @param url
     */
    public void loadBitmap(ImageView imageView, String url) {
        Bitmap bitmap = getBitmapFormCache(mLurCache, url);
        if (bitmap == null) {
            BitmapWorkTask task = new BitmapWorkTask(mLurCache, mDisLruCache, tasks, mGridView);
            tasks.add(task);
            task.execute(url);
        } else {
            if (imageView != null && bitmap != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    private void init() {

        tasks = new HashSet<BitmapWorkTask>();
        //获取程序可以得到的最大的内存
        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        //缓存图片内存的大小
        int cache = maxMemory / 8;
        mLurCache = new LruCache<String, Bitmap>(cache) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getByteCount();
            }
        };

        File cacheDir = CacheUtil.getDiskCacheDir(mContext, "temp");
        try {
            if (!cacheDir.exists()) {
                LogUtil.d("在磁盘上创建缓存空间");
                cacheDir.mkdirs();
            }
            mDisLruCache = DiskLruCache.open(cacheDir, ApkUtil.getAppVersion(mContext), 1, 10 * 1024 * 1024);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 取消所有的下载线程，当activity destroy时调用
     */
    public void cancelTasks() {
        if (tasks != null) {
            for (BitmapWorkTask task : tasks) {
                task.cancel(false);
            }
        }
    }

    /**
     * 设置item子项的高度。
     */
    public void setItemHeight(int height) {
        if (height == mItemHeight) {
            return;
        }
        mItemHeight = height;
        notifyDataSetChanged();
    }

    /**
     * 将缓存记录同步到journal文件中去
     */
    public void flushCache() {
        if (mDisLruCache != null) {
            try {
                mDisLruCache.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
