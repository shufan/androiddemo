package com.example.heshufan.template.module.refrence;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;

import com.example.common.util.LogUtil;
import com.example.heshufan.template.aidl.ICompute;
import com.example.heshufan.template.aidl.ISecurityCenter;
import com.example.heshufan.template.binderpool.BinderPool;
import com.example.heshufan.template.binderpool.ComputeImp;
import com.example.heshufan.template.binderpool.EntryImp;
import com.example.heshufan.template.R;

import butterknife.ButterKnife;

/**
 * 利用BinderPool进行跨进程Binder管理
 * @author heshufan
 */
public class BinderPoolActivity extends BaseActivity {
    private static final String TAG = "BinderPoolActivity:";
    private ICompute addProxy ;
    private ISecurityCenter encryProxy ;
    private BinderPool mBinderPool;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);
        ButterKnife.bind(this);

        //绑定Service与Binder方法的调用最好不要放在主线程
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    doWork();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void doWork() throws RemoteException {
        LogUtil.d("BinderPool");
        mBinderPool = BinderPool.getInsance(BinderPoolActivity.this);

        IBinder service = mBinderPool.queryBinder(0);

        addProxy = ComputeImp.asInterface(service);
        addProxy.add(1,2);

        IBinder service1 = mBinderPool.queryBinder(1);
        encryProxy = EntryImp.asInterface(service1);
        encryProxy.encrypt("helloworld-安卓");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
