package com.example.heshufan.template.module.refrence.mvp.view;

import com.example.heshufan.template.module.refrence.mvp.presenter.BasePresenter;

/**
 * @author heshufan
 * @date 2019/3/15
 */

public interface BaseView <P extends BasePresenter> {
    void setPresenter(P presenter);
}
