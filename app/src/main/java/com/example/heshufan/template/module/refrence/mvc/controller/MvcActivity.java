package com.example.heshufan.template.module.refrence.mvc.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.example.apt_annotation.MyBindView;
import com.example.common.util.BindViewTools;
import com.example.common.util.LogUtil;
import com.example.heshufan.template.R;
import com.example.heshufan.template.module.refrence.BaseActivity;
import com.example.heshufan.template.module.refrence.mvc.CallBack.CallBack1;
import com.example.heshufan.template.module.refrence.mvc.model.MvcModel;
import com.example.heshufan.template.module.refrence.net.bean.MyJoke;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * @author heshufan
 * @date 2019/3/15
 */

public class MvcActivity extends BaseActivity {
    private MvcModel mvcModel;

    @MyBindView(R.id.mvcReturn)
    public TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvc);
        BindViewTools.bind(this);
        mvcModel = new MvcModel();
        getDataByModel();
    }

    private void getDataByModel() {
        mvcModel.getDataByRxJava2(new CallBack1<List<MyJoke>>() {
            @Override
            public void CallBack(List<MyJoke> myJokes) {
                textView.setText(myJokes.get(0).getContent());
            }
        });
    }
}
