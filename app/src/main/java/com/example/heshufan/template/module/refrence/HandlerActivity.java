package com.example.heshufan.template.module.refrence;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.apt_annotation.MyBindView;
import com.example.common.util.BindViewTools;
import com.example.heshufan.template.R;
import com.example.heshufan.template.handler.MyHandler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author heshufan
 * Handler
 */
public class HandlerActivity extends BaseActivity {

    //显示子线程发过来的消息
    @MyBindView(R.id.showMsgFromChildThread)
    public TextView showMsgTV;

    //发送给子线程的消息
    @MyBindView(R.id.msgToChildThread)
    public EditText msgET;

    //开启子线程，不断发送消息到主线程显示
    @MyBindView(R.id.startChildThreadBtn)
    public Button startChildThread;

    //发送消息到子线程
    @MyBindView(R.id.sendMsgBtn)
    public Button sendMsgToChildThreadBtn;

    private static final int FINISH_CHILD_THREAD = 100;
    private static final int CHILD_THREAD_EXCEPTION = 101;

    //主线程中用来处理其他线程发过来的消息的handler
    private MyHandler mHandler;

    //接受消息的子线程
    private ReceiveMsgThread receMsgThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handler);
        BindViewTools.bind(this);

        //开启接受消息的子线程
        receMsgThread = new ReceiveMsgThread();
        receMsgThread.start();

        sendMsgToChildThreadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = msgET.getText().toString();
                Message message = new Message();
                Bundle bundle = new Bundle();
                bundle.putString("msg",msg);
                message.what = 1;
                message.setData(bundle);
                receMsgThread.handler.sendMessage(message);

            }
        });

        startChildThread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //开启发送消息的子线程
                ExecutorService exec = Executors.newCachedThreadPool();
                exec.execute(new SubThread());
            }
        });

        //处理其他线程传过来的消息
        mHandler = new MyHandler(this);
    }

    /**
     * 子线程发送消息到主线程更新textview
     */
    class SubThread implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                mHandler.obtainMessage(0).sendToTarget();
                try {
                    //休眠
                    TimeUnit.MILLISECONDS.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    mHandler.obtainMessage(CHILD_THREAD_EXCEPTION).sendToTarget();
                }
            }
            //当线程结束时通知主线程
            mHandler.obtainMessage(FINISH_CHILD_THREAD).sendToTarget();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }

    /**
     * 接受主线程消息的子线程
     */
    static class ReceiveMsgThread extends Thread {
        private static Handler handler;

        @Override
        public void run() {
            super.run();
            //创建线程的looper与messageQueue
            Looper.prepare();
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    //子线程的handler进行处理消息
                    switch (msg.what) {
                        case 1:
                            Log.i("heshufan", msg.getData().getString("msg"));
                            break;
                        default:
                            break;
                    }
                }
            };
            //开启消息队列的循环
            Looper.loop();
        }
    }
}
