package com.example.heshufan.template.module.refrence.mvp.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.example.apt_annotation.MyBindView;
import com.example.common.util.BindViewTools;
import com.example.heshufan.template.R;
import com.example.heshufan.template.module.refrence.BaseActivity;
import com.example.heshufan.template.module.refrence.mvc.CallBack.CallBack1;
import com.example.heshufan.template.module.refrence.mvp.model.MvpContract;
import com.example.heshufan.template.module.refrence.net.bean.MyJoke;

import java.util.List;

/**
 * @author heshufan
 * @date 2019/3/15
 */

public class MvpActivity extends BaseActivity implements MvpContract.MvpView {
    private MvpContract.MvpPresenter mvpPresenter;

    @MyBindView(R.id.mvpReturn)
    public TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvp);
        BindViewTools.bind(this);

        //调用接口中的方法
        setPresenter(new MvpContract.MvpPresenter());

        mvpPresenter.getDataByRxJava2(new CallBack1<List<MyJoke>>() {
            @Override
            public void CallBack(List<MyJoke> myJokes) {
                setDataToView(myJokes);
            }
        });
    }

    //????todo
    @Override
    public void setPresenter(MvpContract.MvpPresenter presenter) {
        this.mvpPresenter = presenter;
    }


    @Override
    public void setDataToView(List<MyJoke> myJokes) {
        textView.setText(myJokes.get(0).getContent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mvpPresenter.onDestory();
    }
}
