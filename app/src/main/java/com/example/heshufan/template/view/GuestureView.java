package com.example.heshufan.template.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

/**
 * describe:一个自定义View示例，并添加点击事件
 *
 * @author heshufan
 */
public class GuestureView extends View {

    private Canvas mCanvas;
    private Paint mPaint;
    private int defaultSize = 800;
    private GestureDetector.SimpleOnGestureListener listener;
    private GestureDetector detector;

    public GuestureView(Context context) {
        super(context);
    }

    public GuestureView(final Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStrokeWidth(2);
        mPaint.setColor(Color.BLUE);
        mPaint.setStyle(Paint.Style.STROKE);

        //双击检测 监听器
        listener = new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                Toast.makeText(context.getApplicationContext(), "双击666", Toast.LENGTH_SHORT).show();
                return super.onDoubleTap(e);
            }
        };

        detector = new GestureDetector(context, listener);

    }

    public GuestureView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(setMeasure(widthMeasureSpec), setMeasure(heightMeasureSpec));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mCanvas = canvas;
        mCanvas.drawCircle(400, 400, 80, mPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        detector.onTouchEvent(event);
        return true;
    }

    private int setMeasure(int spec) {
        int mode = MeasureSpec.getMode(spec);
        int size = MeasureSpec.getSize(spec);
        if (mode == MeasureSpec.EXACTLY) {
            return size;
        } else if (mode == MeasureSpec.AT_MOST) {
            return Math.min(size, defaultSize);
        } else {
            return defaultSize;
        }
    }
}
