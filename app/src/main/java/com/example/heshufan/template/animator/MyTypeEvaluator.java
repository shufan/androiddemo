package com.example.heshufan.template.animator;

/**
 * @author heshufan
 * @date 2019/5/7
 */

import android.animation.TypeEvaluator;

/**
 * 自定义属性动画的估值算法，图片放大
 */
public class MyTypeEvaluator implements TypeEvaluator<ViewPoint> {
    @Override
    public ViewPoint evaluate(float fraction, ViewPoint startValue, ViewPoint endValue) {
        ViewPoint point = new ViewPoint();
        float value = fraction * fraction;
        value = (float) Math.pow(value, 0.5);
        point.translateX = startValue.translateX + (endValue.translateX - startValue.translateX) * value;
        point.translateY = startValue.translateY + (endValue.translateY - startValue.translateY) * value;
        point.sizePoint.x = startValue.sizePoint.x + (endValue.sizePoint.x - startValue.sizePoint.x) * value;
        point.sizePoint.y = startValue.sizePoint.y + (endValue.sizePoint.y - startValue.sizePoint.y) * value;
        return point;
    }
}
