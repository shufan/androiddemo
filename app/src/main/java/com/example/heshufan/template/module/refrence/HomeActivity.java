package com.example.heshufan.template.module.refrence;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apt_annotation.MyBindView;
import com.example.common.util.BindViewTools;
import com.example.heshufan.template.R;


/**
 * @author heshufan
 *         自定义View
 */
public class HomeActivity extends BaseActivity implements View.OnClickListener {

    @MyBindView(R.id.homeTitle)
    public TextView homeTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_home);
        BindViewTools.bind(this);
        homeTitle.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.homeTitle:
                Toast.makeText(this,"注解处理器绑定",Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }
}
