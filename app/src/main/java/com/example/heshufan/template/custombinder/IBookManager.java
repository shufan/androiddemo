//package com.example.heshufan.template.custombinder;

/**
 * 手动实现binder
 * @author heshufan
 * @date 2019/4/9
 */

//public interface IBookManager extends IInterface {
//    //Binder的唯一标识
//    static final String DESCRIPTION = "com.example.heshufan.template.com.example.heshufan.aidl.IBookManager";
//
//    //标记方法
//    static final int TRANSACTION_getBookList = IBinder.FIRST_CALL_TRANSACTION + 0;
//    static final int TRANSACTION_addBook = IBinder.FIRST_CALL_TRANSACTION + 1;
//
//    //获取图书列表
//    public List<Book> getBookList() throws RemoteException;
//
//    //添加书本
//    public void addBook(Book book) throws RemoteException;
//}
