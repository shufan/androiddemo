package com.example.heshufan.template.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;

import com.example.common.util.LogUtil;
import com.example.heshufan.template.animator.MyTypeEvaluator;
import com.example.heshufan.template.animator.ViewPoint;
import com.example.heshufan.template.util.ViewUtils;

/**
 * @author heshufan
 * @date 2019/5/6
 */

public class SlideCloseView extends View implements GestureDetector.OnDoubleTapListener, GestureDetector.OnGestureListener {
    private GestureDetector mDetector;

    //初始View的位置
    private Rect startRect;
    //view实时的位置
    private Rect viewRect;

    //初始左上坐标
    private PointF startPosition;
    //当前左上坐标
    private PointF currentPosition;

    private Bitmap mBitmap;

    private Paint mPaint;

    //铺满全屏时放大的倍数
    private float maxScale;

    private int defaultSize = 400;

    private float mHeight, mWidth;

    //图片缩放动画
    private ValueAnimator mAnimator;
    //动画的时间
    private long time;

    /**
     * 初始化view
     *
     * @param w        宽
     * @param h        高
     * @param bitmap   图片
     * @param location 左上角位置
     */
    public void createOriginView(int w, int h, Bitmap bitmap, int[] location) {
        startRect.right = w;
        startRect.bottom = h;

//        viewRect.right = w;
//        viewRect.bottom = h;

        startPosition.x = location[0];
        startPosition.y = location[1] - ViewUtils.getStatusBarHeight(getContext());
        currentPosition.x = location[0];
        currentPosition.y = location[1] - ViewUtils.getStatusBarHeight(getContext());
        mBitmap = bitmap;
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        maxScale = 2;

    }

    /**
     * 初始化数据
     */
    private void initView() {

        mDetector = new GestureDetector(getContext(), this);
        startRect = new Rect();
        startPosition = new PointF();
        currentPosition = new PointF();
    }


    //加载图片的时候开始动画进行放大铺满屏幕
//    @Override
//    protected void onAttachedToWindow() {
//        super.onAttachedToWindow();
//        //post可以将一个Runnable抛到消息队列的末尾，等到view已经初始化好了后进行调用
//        post(new Runnable() {
//            @Override
//            public void run() {
//                //view需要全屏 这里是屏幕的大小
////                mWidth = getWidth();
////                mHeight = getHeight();
//
//                float width = startRect.width();
//                float height = startRect.height();
//
//                float maxScaleX = mWidth / width;
//                float maxScaleY = mHeight / height;
//                maxScale = Math.max(maxScaleX, maxScaleY);
//
//                //1080*1845
//                LogUtil.ds("mWidth:" + mWidth, "mHeight:" + mHeight);
//                //600*600
//                LogUtil.ds("width:" + width, "height:" + height);
//                //1080*1845
//                LogUtil.ds("getMeasuredWidth:" + getMeasuredWidth(), "getMeasuredHeight:" + getMeasuredHeight());
//                //1920*1920
//                LogUtil.ds("mBitmap.getWidth():" + mBitmap.getWidth(), "mBitmap.getHeight():" + mBitmap.getHeight());
//
//            }
//        });
//    }

    /**
     * 计算动画初始点与终点
     */
    private void startAnimotion(){
        //起始数据
        float startTranslatY = startPosition.y;
        float stattTranslatX = startPosition.x;
        int right = startRect.right;
        int bottom = startRect.bottom;
        PointF pointF = new PointF();
        pointF.x = right;
        pointF.y = bottom;
        ViewPoint startPoint = new ViewPoint(stattTranslatX, startTranslatY, pointF);

        //结束数据
        float width = mWidth;
        float height = mHeight;
        double v = mBitmap.getWidth() * 1.0 / mBitmap.getHeight();
        float endTranslatX = 0;
        float endTranslatY = 0;

        if (mWidth / mHeight > v) {
            //高铺满屏幕
            //宽与高比例等于view的宽与高的比例
            width = (float) (mHeight * v);
            endTranslatX = (mWidth - width) / 2;
        } else {
            //宽铺满屏幕
            //宽与高比例等于view的宽与高的比例
            height = (float) (width / v);
            endTranslatY = (mHeight - height) / 2;
        }

        PointF pointF1 = new PointF(width, height);
        ViewPoint endPoint = new ViewPoint(endTranslatX, endTranslatY, pointF1);
        getAnimotion(startPoint,endPoint,false);
    }

    /**
     * 开始动画
     * @param startPoint 开始
     * @param endPoint 结束
     * @param miss 是否退出大图浏览
     */
    private void getAnimotion(ViewPoint startPoint, ViewPoint endPoint, final boolean miss) {
        clear();
        //对对象进行计算的属性动画，这里是ViewPoint对象
        mAnimator = ValueAnimator.ofObject(new MyTypeEvaluator(), startPoint, endPoint);
        mAnimator.setInterpolator(new DecelerateInterpolator());
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            //动画进行过程中的回调
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewPoint point = (ViewPoint) animation.getAnimatedValue();
                viewRect.right = (int) point.sizePoint.x;
                viewRect.left = 0;
                viewRect.top = 0;
                viewRect.bottom = (int) point.sizePoint.y;
//                currentPoint.y = point.translateY;
//                currentPoint.x = point.translateX;
                postInvalidate();
            }
        });
        //动画结束时的回调
        mAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
//                if (listener != null && miss) {
//                    listener.animotionEnd(SlideCloseView.this);
//                }
                LinearLayout linearLayout = (LinearLayout) getParent();
                if (null != linearLayout) {
                    linearLayout.getBackground().mutate().setAlpha(0);
                }
                if (miss) {
                    ((Activity) (getContext())).finish();
                    ((Activity) (getContext())).overridePendingTransition(0, 0);
                }
            }
        });
        mAnimator.setDuration(time);
        mAnimator.start();
    }

    private void clear() {
        if (mAnimator != null) {
            mAnimator.cancel();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mDetector.onTouchEvent(event);
        return true;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        LogUtil.d("mBitmap null");
        if (mBitmap != null) {
            LogUtil.d("mBitmap");
            mPaint.setColor(Color.TRANSPARENT);
//            canvas.drawRect(0, 0, mWidth, mHeight, mPaint);
            canvas.save();
            canvas.translate(startPosition.x, startPosition.y);
            Bitmap bitmap = creatBitmap(mBitmap, startRect);
            //600*600
            LogUtil.ds("bitmap.getWidth():" + bitmap.getWidth(), "bitmap.getHeight():" + bitmap.getHeight());
            canvas.drawBitmap(bitmap, 0, 0, null);
            canvas.restore();
        }

    }

    /**
     * scale bitmap
     *
     * @param bitmap
     * @param viewRect
     * @return
     */
    private Bitmap creatBitmap(Bitmap bitmap, Rect viewRect) {
        float width = viewRect.width();
        float height = viewRect.height();
        LogUtil.ds("width:" + width, "height:" + height);
        return Bitmap.createScaledBitmap(bitmap, (int) width, (int) height, true);
    }

    //OnDoubleTapListener:单击事件
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        LogUtil.d("onSingleTapConfirmed");
        return false;
    }


    //OnDoubleTapListener:双击事件
    @Override
    public boolean onDoubleTap(MotionEvent e) {
        LogUtil.d("onDoubleTap");
        return false;
    }

    //OnDoubleTapListener：双击事件
    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        LogUtil.d("onDoubleTapEvent");

        return false;
    }

    //OnGestureListener:按下
    @Override
    public boolean onDown(MotionEvent e) {
        LogUtil.d("onDown");

        return false;
    }

    //OnGestureListener：按下但没动，主要是用来反馈给用户已经按成功了
    @Override
    public void onShowPress(MotionEvent e) {
        LogUtil.d("onShowPress");


    }

    //OnGestureListener：
    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        LogUtil.d("onSingleTapUp");
        return false;
    }

    //OnGestureListener：滑动
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        LogUtil.d("onScroll");

        return false;
    }

    //OnGestureListener：长按
    @Override
    public void onLongPress(MotionEvent e) {
        LogUtil.d("onLongPress");

    }

    //OnGestureListener：抛、丢
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        LogUtil.d("onFling");

        return false;
    }

    public SlideCloseView(Context context) {
        super(context);
        initView();
    }

    public SlideCloseView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public SlideCloseView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(setMeasure(widthMeasureSpec), setMeasure(heightMeasureSpec));
    }

    private int setMeasure(int spec) {
        int mode = MeasureSpec.getMode(spec);
        int size = MeasureSpec.getSize(spec);
        if (mode == MeasureSpec.EXACTLY) {
            LogUtil.d("size1:" + size);
            return size;
        } else if (mode == MeasureSpec.AT_MOST) {
            LogUtil.d("size2:" + Math.min(size, defaultSize));
            return Math.min(size, defaultSize);
        } else {
            return defaultSize;
        }
    }
}
