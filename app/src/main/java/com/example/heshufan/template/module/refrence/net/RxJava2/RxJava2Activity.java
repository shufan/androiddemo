package com.example.heshufan.template.module.refrence.net.RxJava2;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.common.util.LogUtil;
import com.example.heshufan.template.R;
import com.example.heshufan.template.module.refrence.BaseActivity;
import com.example.heshufan.template.module.refrence.net.bean.MyJoke;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author heshufan
 * @date 2019/3/12
 */

public class RxJava2Activity extends BaseActivity {
    private Retrofit retrofit;
    private static final int TIME_OUT = 4;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_net);
        initRetrofit();
        Api api = retrofit.create(Api.class);
        Observable<List<MyJoke>> observable = api.getData();
        observable.observeOn(AndroidSchedulers.mainThread())
                //事件处理的线程
                .subscribeOn(Schedulers.io())
                //事件发生的线程
                .subscribe(new Observer<List<MyJoke>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        LogUtil.d("onsubcribe");
                    }

                    @Override
                    public void onNext(List<MyJoke> myJokes) {
                        LogUtil.d("onNext");
                        for (MyJoke myJoke : myJokes) {
                            LogUtil.d(myJoke.getContent());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtil.d(e.toString());
                    }

                    @Override
                    public void onComplete() {
                        LogUtil.d("onComplete");
                    }
                });
    }

    private void initRetrofit() {
        OkHttpClient client = new OkHttpClient();
        client.newBuilder().connectTimeout(TIME_OUT, TimeUnit.SECONDS);
        retrofit = new Retrofit.Builder()
                .baseUrl("http://api.laifudao.com/open/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }
}
