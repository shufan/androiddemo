package com.example.heshufan.template.module.refrence.net.bean;

/**
 * @author heshufan
 * @date 2019/3/7
 */

public class Translation {
    private String errorCode;
    private String l;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }
}
