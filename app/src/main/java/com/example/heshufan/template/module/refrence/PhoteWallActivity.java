package com.example.heshufan.template.module.refrence;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewTreeObserver;
import android.widget.GridView;

import com.example.heshufan.template.R;
import com.example.heshufan.template.adapter.PhotoWallAdapter;
import com.example.heshufan.template.constant.Images;

/**
 * @author heshufan
 * @date 2019/2/20
 */

public class PhoteWallActivity extends AppCompatActivity {

    private GridView mGridView;
    private PhotoWallAdapter adapter;
    private int mImageThumbSize;
    private int mImageThumbSpacing;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_wall);

        mGridView = findViewById(R.id.photoWallGV);
        adapter = new PhotoWallAdapter(this,0, Images.imagesUrls,mGridView);
        mGridView.setAdapter(adapter);
        mImageThumbSize = getResources().getDimensionPixelSize(
                R.dimen.image_thumbnail_size);
        mImageThumbSpacing = getResources().getDimensionPixelSize(
                R.dimen.image_thumbnail_spacing);

        mGridView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        final int numColumns = (int) Math.floor(mGridView
                                .getWidth()
                                / (mImageThumbSize + mImageThumbSpacing));
                        if (numColumns > 0) {
                            int columnWidth = (mGridView.getWidth() / numColumns)
                                    - mImageThumbSpacing;
                            adapter.setItemHeight(columnWidth);
                            mGridView.getViewTreeObserver()
                                    .removeGlobalOnLayoutListener(this);
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        switch (i) {
            case R.id.one:
                Intent intent1 = new Intent(this,HandlerActivity.class);
                startActivity(intent1);
                break;
            case R.id.two:
                Intent intent2 = new Intent(this,AnimationActivity.class);
                startActivity(intent2);
                break;
            case R.id.three:
                Intent intent3 = new Intent(this,BitmapActivity.class);
                startActivity(intent3);
                break;
            case R.id.four:
                Intent intent4 = new Intent(this,ViewActivity.class);
                startActivity(intent4);
                break;
            case R.id.five:
                break;
            case R.id.six:
                break;
            case R.id.seven:
                break;
            case R.id.eight:
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        adapter.flushCache();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.cancelTasks();
    }
}
