package com.example.heshufan.template.aidl;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 对象在不同的进程之间进行传递需要继承Parcelable，方便转化回Book对象,可以在Binder中自由的传输
 *
 * @author heshufan
 * @date 2019/4/9
 */

public class Book implements Parcelable {
    private int bookId;
    private String bookName;

    public Book(int bookId, String bookName) {
        this.bookId = bookId;
        this.bookName = bookName;
    }

    //反序列化
    private Book(Parcel in){
        bookId = in.readInt();
        bookName = in.readString();
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    //序列化
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(bookId);
        dest.writeString(bookName);
    }

    //反序列化
    public static final Parcelable.Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel source) {
            return new Book(source);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };
}
