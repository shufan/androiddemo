package com.example.heshufan.template.module.refrence;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.example.heshufan.template.R;

/**
 * @author heshufan
 * @date 2019/3/12
 */

public class GlideActivity extends BaseActivity implements View.OnClickListener{
    Button mButton;
    private ImageView mImageView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glide);
        initView();
    }
    private void initView(){
        mButton = findViewById(R.id.getImg);
        mButton.setOnClickListener(this);
        mImageView = findViewById(R.id.image);
    }

    @Override
    public void onClick(View v) {
        getImage(mImageView);
    }

    private void getImage(ImageView imageView){

        //拿到图片后的回调
        SimpleTarget<Drawable> simpleTarget = new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                //todo
            }
        };

        //预加载
        Glide.with(this)
                .load("http://guolin.tech/book.png")
                .preload();
        Glide.with(this)
                .load("http://guolin.tech/book.png")
                .into(imageView);

        //listener
        RequestListener<Drawable> requestListener = new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        };
        Glide.with(this)
                .load("http://guolin.tech/book.png")
                .listener(requestListener)
                .into(imageView);

        //占位图
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.image_place_holder)
                //清除硬盘缓存
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                //下载错误图
                .error(R.drawable.testbitmap)
                //指定图片大小
                .override(200,100)
                //不用内存缓存
                .skipMemoryCache(true);
        String url = "http://guolin.tech/book.png";
        Glide.with(this)
                .load(url)
                .apply(options)
                .into(imageView);

        //直接加载gif
        Glide.with(this)
                .load("http://guolin.tech/test.gif")
                .into(imageView);
    }
}
