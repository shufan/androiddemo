//package com.example.heshufan.template.custombinder;
//
//import android.os.Binder;
//import android.os.IBinder;
//import android.os.IInterface;
//import android.os.Parcel;
//import android.os.RemoteException;
//
//import com.example.heshufan.template.aidl.Book;
//
//import java.util.List;
//
///**
// * @author heshufan
// * @date 2019/4/9
// */
//
//public class IBookManagerImpl extends Binder implements IBookManager {
//
//    //todo
//    public IBookManagerImpl() {
//        this.attachInterface(this, DESCRIPTION);
//    }
//
//    /**
//     * 运行在远程服务器的线程中
//     *
//     * @param code  调用方法的ID
//     * @param data  客户端传入的参数
//     * @param reply 服务端返回的数据
//     * @param flags 标志
//     * @return 返回false表示客户端会请求失败
//     * @throws RemoteException
//     */
//    @Override
//    protected boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
//        switch (code) {
//            case INTERFACE_TRANSACTION: {
//                reply.writeString(DESCRIPTION);
//                return true;
//            }
//            case TRANSACTION_getBookList:
//                data.enforceInterface(DESCRIPTION);
//                List<Book> result = this.getBookList();
//                reply.writeNoException();
//                reply.writeTypedList(result);
//                return true;
//            case TRANSACTION_addBook:
//                data.enforceInterface(DESCRIPTION);
//                Book arg0;
//                if (0 != data.readInt()) {
//                    //反序列化
//                    arg0 = Book.CREATOR.createFromParcel(data);
//                } else {
//                    arg0 = null;
//                }
//                this.addBook(arg0);
//                reply.writeNoException();
//                return true;
//
//        }
//        return super.onTransact(code, data, reply, flags);
//    }
//
//    @Override
//    public List<Book> getBookList() throws RemoteException {
//        //todo
//        return null;
//    }
//
//    @Override
//    public void addBook(Book book) throws RemoteException {
//        //todo
//
//    }
//
//    //将服务端的Binder对象转换为客户端所需的AIDL对象
//    public static IBookManager asInterface(IBinder obj) {
//        if (obj == null) {
//            return null;
//        }
//        //客户端和服务端在一个进程
//        IInterface iInterface = obj.queryLocalInterface(DESCRIPTION);
//        if (iInterface != null && iInterface instanceof IBookManager) {
//            return (IBookManager) iInterface;
//        }
//        //客户端和服务端不在一个进程
//        return new IBookManagerImpl.Proxy(obj);
//
//    }
//
//    //返回当前的Binder对象
//    @Override
//    public IBinder asBinder() {
//        return null;
//    }
//
//    //代理类
//    public static class Proxy implements IBookManager {
//        //远程Binder，以此来调用远程的方法
//        IBinder mRemote;
//
//        public Proxy(IBinder mRemote) {
//            this.mRemote = mRemote;
//        }
//
//        //todo
//        public String getInterfaceDescription() {
//            return DESCRIPTION;
//        }
//
//        //方法运行在客户端
//        @Override
//        public List<Book> getBookList() throws RemoteException {
//            Parcel data = Parcel.obtain();
//            Parcel reply = Parcel.obtain();
//            List<Book> result;
//
//            try{
//                data.writeInterfaceToken(DESCRIPTION);
//                //调用远程的方法
//                mRemote.transact(TRANSACTION_getBookList, data, reply, 0);
//                //返回的数据
//                reply.readException();
//                result = reply.createTypedArrayList(Book.CREATOR);
//
//            } finally {
//                reply.recycle();
//                data.recycle();
//            }
//            return result;
//        }
//
//        //方法运行在客户端
//        @Override
//        public void addBook(Book b) throws RemoteException {
//            Parcel data = Parcel.obtain();
//            Parcel reply = Parcel.obtain();
//            List<Book> result;
//
//            try{
//                data.writeInterfaceToken(DESCRIPTION);
//                //调用远程的方法
//                mRemote.transact(TRANSACTION_getBookList, data, reply, 0);
//                //返回的数据
//                reply.readException();
//            } finally {
//                reply.recycle();
//                data.recycle();
//            }
//        }
//
//        @Override
//        public IBinder asBinder() {
//            return mRemote;
//        }
//    }
//}
