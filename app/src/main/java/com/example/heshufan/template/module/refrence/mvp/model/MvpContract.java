package com.example.heshufan.template.module.refrence.mvp.model;

import com.example.common.util.LogUtil;
import com.example.heshufan.template.module.refrence.mvc.CallBack.CallBack1;
import com.example.heshufan.template.module.refrence.mvp.presenter.BasePresenter;
import com.example.heshufan.template.module.refrence.mvp.view.BaseView;
import com.example.heshufan.template.module.refrence.net.RxJava2.Api;
import com.example.heshufan.template.module.refrence.net.bean.MyJoke;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author heshufan
 * @date 2019/3/15
 */

public class MvpContract {

    public static class MvpPresenter implements BasePresenter {

        public void getDataByRxJava2(final CallBack1<List<MyJoke>> callBack1) {
            OkHttpClient client = new OkHttpClient();
            client.newBuilder().connectTimeout(4, TimeUnit.SECONDS);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://api.laifudao.com/open/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .build();
            Api api = retrofit.create(Api.class);
            Observable<List<MyJoke>> observable = api.getData();
            observable.observeOn(AndroidSchedulers.mainThread())
                    //事件处理的线程
                    .subscribeOn(Schedulers.io())
                    //事件发生的线程
                    .subscribe(new Observer<List<MyJoke>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(List<MyJoke> myJokes) {
                            callBack1.CallBack(myJokes);
                        }

                        @Override
                        public void onError(Throwable e) {
                            LogUtil.d(e.toString());
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }

        @Override
        public void onDestory() {

        }
    }

    /**
     * MvpPreserter指定泛型具体是哪个子类，可以服用
     */
    public interface MvpView extends BaseView<MvpPresenter> {
        void setDataToView(List<MyJoke> myJokes);
    }
}
