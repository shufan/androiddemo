package com.example.heshufan.template.module.refrence.mvc.CallBack;

/**
 * @author heshufan
 * @date 2019/3/15
 * 接口，利用回调来将数据又model传到Controller
 */

public interface CallBack1<T> {
    void CallBack(T t);
}
