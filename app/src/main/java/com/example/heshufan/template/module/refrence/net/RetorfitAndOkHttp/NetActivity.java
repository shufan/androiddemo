package com.example.heshufan.template.module.refrence.net.RetorfitAndOkHttp;

import android.os.Bundle;

import com.example.common.util.LogUtil;
import com.example.heshufan.template.R;
import com.example.heshufan.template.module.refrence.BaseActivity;
import com.example.heshufan.template.module.refrence.net.bean.Translation;
import com.example.heshufan.template.module.refrence.net.bean.Translation1;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Action1;


public class NetActivity extends BaseActivity {
    private Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_net);
        initRetrofit();
        getTop250();
        initRX();
    }

    /**
     * 测试rxjava
     */
    private void initRX() {
        /**
         * 1、最简单的实例
         */
        //观察者
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String s) {
                LogUtil.d(s);
            }
        };
        //被观察者
        Observable<String> observable = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("I");
                subscriber.onNext("Love");
                subscriber.onNext("You");
                subscriber.onCompleted();
            }
        });
        //订阅
        observable.subscribe(observer);

        /**
         * 2、正常的使用,完整的对调 onNext，onError,OnNext
         */
        Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("heshufan");
                subscriber.onCompleted();
            }
        }).subscribe(new Observer<String>() {
            @Override
            public void onCompleted() {
                LogUtil.d("finish");
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String s) {
                LogUtil.d(s);
            }
        });

        /**
         * 3、正常的使用,不完整的回调,回调放在了onNextAction
         */
        //正常回调
        Action1<String> onNextAction = new Action1<String>() {
            @Override
            public void call(String s) {
                LogUtil.d(s);
            }
        };
        //错误回调
        Action1<Throwable> onThrowableAction = new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {

            }
        };
        Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("heshufan");
                subscriber.onCompleted();
            }
        }).subscribe(onNextAction);
    }

    /**
     * 测试retrofit
     */
    private void getTop250() {
        MovieService service = retrofit.create(MovieService.class);
        Call<Translation> call = service.getCallByHttp();
        call.enqueue(new Callback<Translation>() {
            @Override
            public void onResponse(Call<Translation> call, Response<Translation> response) {
                LogUtil.d(response.body().getErrorCode());
            }

            @Override
            public void onFailure(Call<Translation> call, Throwable t) {
                LogUtil.d(t.getMessage());
            }
        });
        Call<Translation1> call1 = service.getCall("I Love You");
        call1.enqueue(new Callback<Translation1>() {
            @Override
            public void onResponse(Call<Translation1> call, Response<Translation1> response) {
                LogUtil.d(response.body().getTranslateResult().get(0).get(0).getTgt());
            }

            @Override
            public void onFailure(Call<Translation1> call, Throwable t) {

            }
        });

    }

    private void initRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://fanyi.youdao.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }
}
