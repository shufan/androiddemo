package com.example.heshufan.template.handler;

import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

import com.example.heshufan.template.module.refrence.HandlerActivity;

import java.lang.ref.WeakReference;

/**
 * 主线程的handler，处理其他线程传过来的消息
 * @author heshufan
 * @date 2019/2/19
 */

public class MyHandler extends Handler {
    private final WeakReference<HandlerActivity> mActivity;

    public MyHandler(HandlerActivity activity) {
        mActivity = new WeakReference<>(activity);
    }

    @Override
    public void handleMessage(Message msg) {
        TextView textView = mActivity.get().showMsgTV;
        switch (msg.what) {
            case 0:
                textView.setText(textView.getText().toString()+"\n"+"获取到来自子线程的消息为：" + msg.what);
                break;
            case 100:
                textView.setText(textView.getText().toString() + "\n" + ":" + "子线程已执行完毕");
                break;
            case 101:
                textView.setText(textView.getText().toString() + "\n" + ":" + "子线程出现异常");
            default:
                break;
        }
    }
}
