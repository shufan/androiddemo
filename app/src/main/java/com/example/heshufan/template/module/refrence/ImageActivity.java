package com.example.heshufan.template.module.refrence;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.common.util.LogUtil;
import com.example.common.view.MyClockView;
import com.example.heshufan.template.R;
import com.example.heshufan.template.view.CircleView;
import com.example.heshufan.template.view.GuestureView;
import com.example.heshufan.template.view.SlideCloseView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * wechat
 *
 * @author heshufan
 */
public class ImageActivity extends AppCompatActivity {
    SlideCloseView mSlideCloseView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image);
        ButterKnife.bind(this);
        mSlideCloseView = findViewById(R.id.imageView);
        Intent intent = getIntent();
        int[] location = new int[2];
        location[0] = intent.getIntExtra("x", 0);
        location[1] = intent.getIntExtra("y", 0);
        LogUtil.ds("x:"+location[0],"y:"+location[0]);

        int w = intent.getIntExtra("w", 0);
        int h = intent.getIntExtra("h", 0);
        LogUtil.ds("w:"+w,"h:"+h);

        mSlideCloseView.createOriginView(w, h, BitmapFactory.decodeResource(getResources(), R.drawable.img_userlogo_ameng), location);


    }
}
