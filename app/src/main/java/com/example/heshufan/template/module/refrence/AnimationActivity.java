package com.example.heshufan.template.module.refrence;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.heshufan.template.R;

import butterknife.ButterKnife;

/**
 * @author heshufan
 * 自定义View
 */
public class AnimationActivity extends BaseActivity {
    private ListView listView;
    private String[] strings = new String[]{"heshfuan","heshufan","heshufan","heshufan"};
    private ArrayAdapter arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);
        ButterKnife.bind(this);
        listView = findViewById(R.id.aniamtion_ll);
        arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,strings);
        listView.setAdapter(arrayAdapter);
    }
}
