package com.example.heshufan.template.service;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import com.example.common.util.LogUtil;
import com.example.heshufan.template.aidl.Book;
import com.example.heshufan.template.aidl.BookArrivedLisener;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author heshufan
 * @date 2019/4/12
 */

public class BookManagerService extends Service {

    private static final String TAG = "BMS：";

    private BookManagerImp mBookMangerImp;

    //原子操作的boolean
    private AtomicBoolean mIsServiceDestroyed = new AtomicBoolean(true);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        int access = checkCallingOrSelfPermission("om.example.heshufan.template.ACCESS_BOOK_SERVICE");
        if (access == PackageManager.PERMISSION_DENIED) {
            return mBookMangerImp;
        }
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mBookMangerImp = new BookManagerImp();
        mIsServiceDestroyed.set(true);

        //开启添加书本的线程
        new Thread(new ServiceManager()).start();
    }

    @Override
    public void onDestroy() {
        mIsServiceDestroyed.set(false);
        super.onDestroy();
    }


    /**
     * 定时添加书本并通知客户端
     */
    public class ServiceManager implements Runnable {

        @Override
        public void run() {
            while (mIsServiceDestroyed.get()) {
                try {
                    //间隔时间5s
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                int bookId = 0;
                try {
                    bookId = mBookMangerImp.getBookListBySelf().size() + 1;
                    Book book = new Book(bookId, "new Book #" + bookId);
                    LogUtil.d(TAG + "new Book #" + bookId);
                    //通知客户端
                    OnNewBookArriaved(book);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 添加新书
     *
     * @param book book
     */
    private void OnNewBookArriaved(Book book) throws RemoteException {
        mBookMangerImp.addBookBySelf(book);
        //遍历数组进行通知

        //必须按照此规定来写，不能随意改变
        final int N = mBookMangerImp.getLisentnerList().beginBroadcast();
        for (int i = 0; i < N; i++) {
            BookArrivedLisener lisener = mBookMangerImp.getLisentnerList().getBroadcastItem(i);
            if (lisener != null) {
                lisener.onBookArrived(book);
            }
        }

        mBookMangerImp.getLisentnerList().finishBroadcast();
    }
}
