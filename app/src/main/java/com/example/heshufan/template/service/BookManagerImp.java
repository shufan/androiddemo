package com.example.heshufan.template.service;

import android.os.RemoteCallbackList;
import android.os.RemoteException;

import com.example.common.util.LogUtil;
import com.example.heshufan.template.aidl.Book;
import com.example.heshufan.template.aidl.BookArrivedLisener;
import com.example.heshufan.template.aidl.IBookManager;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author heshufan
 * @date 2019/4/21
 */

public class BookManagerImp extends IBookManager.Stub {
    private static final String TAG = "BookManagerImp";

    //支持并发读写
    private CopyOnWriteArrayList<Book> mBookList;

    //新书上架的监听器
    private RemoteCallbackList<BookArrivedLisener> mLisentnerList;

    public BookManagerImp() {
        mBookList = new CopyOnWriteArrayList<>();
        mLisentnerList = new RemoteCallbackList<>();
        mBookList.add(new Book(1, "Android"));
        mBookList.add(new Book(2, "IOS"));
    }

    @Override
    public List<Book> getBookList() throws RemoteException {
        LogUtil.d(TAG + "返回books");
        return mBookList;
    }

    @Override
    public void addBook(Book book) throws RemoteException {
        LogUtil.d(TAG + book.getBookName());
        mBookList.add(book);
    }

    @Override
    public void setRegisterBookArrivedlistener(BookArrivedLisener listner) throws RemoteException {
        mLisentnerList.register(listner);

    }

    @Override
    public void unRegisterBookArrivedlistener(BookArrivedLisener listner) throws RemoteException {
        mLisentnerList.unregister(listner);
    }

    public CopyOnWriteArrayList<Book> getBookListBySelf() {
        return mBookList;
    }

    public RemoteCallbackList<BookArrivedLisener> getLisentnerList() {
        return mLisentnerList;
    }

    /**
     * 服务端自己添加新书
     * @param book book
     */
    public void addBookBySelf(Book book){
        mBookList.add(book);
    }
}
