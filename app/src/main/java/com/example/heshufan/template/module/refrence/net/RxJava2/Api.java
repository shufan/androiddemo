package com.example.heshufan.template.module.refrence.net.RxJava2;

import com.example.heshufan.template.module.refrence.net.bean.MyJoke;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * @author heshufan
 * @date 2019/3/12
 * RxJava的接口
 */

public interface Api {
    //RxJava2
    @GET("xiaohua.json")
    Observable<List<MyJoke>> getData();
}
