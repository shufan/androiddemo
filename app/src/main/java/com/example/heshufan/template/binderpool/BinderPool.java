package com.example.heshufan.template.binderpool;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import com.example.heshufan.template.aidl.IBinderPool;
import com.example.heshufan.template.service.BinderPoolService;

import java.util.concurrent.CountDownLatch;

/**
 * Binder池管理
 * 1.连接远程Service
 * 2.负责返回不同的Binder给客户端
 *
 * @author heshufan
 * @date 2019/4/22
 */

public class BinderPool {

    private IBinderPool mIBinderPoolImp;
    private static volatile BinderPool sInstance;
    private Context mContext;

    //异步转化为同步
    private CountDownLatch mConnectBinderPoolCountDownLatch;

    public BinderPool(Context context) {
        this.mContext = context.getApplicationContext();
        connectBinderPoolService();
    }

    public static BinderPool getInsance(Context context) {
        if (sInstance == null) {
            synchronized (BinderPool.class) {
                if (sInstance == null) {
                    sInstance = new BinderPool(context);
                }
            }
        }
        return sInstance;
    }

    /**
     * 绑定远程Service
     */
    private synchronized void connectBinderPoolService() {
        mConnectBinderPoolCountDownLatch = new CountDownLatch(1);
        Intent service = new Intent(mContext, BinderPoolService.class);
        mContext.bindService(service, mBindPoolConnection,
                Context.BIND_AUTO_CREATE);
        try {
            mConnectBinderPoolCountDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private ServiceConnection mBindPoolConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mIBinderPoolImp = IBinderPool.Stub.asInterface(service);
            mConnectBinderPoolCountDownLatch.countDown();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    public IBinder queryBinder(int bindCode) throws RemoteException {
        IBinder binder = null;
        try {
            if (mIBinderPoolImp != null) {
                binder = mIBinderPoolImp.queryBinder(bindCode);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return binder;    }


    public static class IBinderPoolImp extends IBinderPool.Stub {

        @Override
        public IBinder queryBinder(int bindCode) throws RemoteException {
            IBinder binder = null;
            switch (bindCode) {
                case 0:
                    binder = new ComputeImp();
                    break;
                case 1:
                    binder = new EntryImp();
                    break;
                default:
                    break;
            }
            return binder;
        }
    }
}
