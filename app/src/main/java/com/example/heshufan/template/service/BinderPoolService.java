package com.example.heshufan.template.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.example.heshufan.template.binderpool.BinderPool;

/**
 * @author heshufan
 * @date 2019/4/22
 */

public class BinderPoolService extends Service {

    private Binder mBinderPool = new BinderPool.IBinderPoolImp();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinderPool;
    }
}
