package com.example.heshufan.template.module.refrence.mvp.presenter;

/**
 * @author heshufan
 * @date 2019/3/15
 */

public interface BasePresenter {
    void onDestory();
}
