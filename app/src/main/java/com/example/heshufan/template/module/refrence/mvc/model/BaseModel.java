package com.example.heshufan.template.module.refrence.mvc.model;

/**
 * @author heshufan
 * @date 2019/3/15
 */

public interface BaseModel {
    void onDestroy();
}
