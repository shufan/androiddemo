package com.example.heshufan.template.binderpool;

import android.os.RemoteException;

import com.example.common.util.LogUtil;
import com.example.heshufan.template.aidl.ISecurityCenter;

/**
 * AIDL接口的实现，也就是运行在服务端的方法
 *
 * @author heshufan
 * @date 2019/4/22
 */

public class EntryImp extends ISecurityCenter.Stub {
    private static final char SECRET_CODE = '^';
    @Override
    public String encrypt(String content) throws RemoteException {
        char[] chars = content.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            chars[i] ^= SECRET_CODE;
        }
        return new String(chars);
    }

    @Override
    public String decrypt(String password) throws RemoteException {
        return encrypt(password);
    }
}
