package com.example.heshufan.template.module.refrence.net.RetorfitAndOkHttp;

import com.example.heshufan.template.module.refrence.net.bean.Translation;
import com.example.heshufan.template.module.refrence.net.bean.Translation1;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * @author heshufan
 * @date 2019/3/7
 */

public interface MovieService {


    //1.请求方式

    //尾址：完整地址：baseUrl+尾址 不带参数
    @GET("top250")
    Call<String> getTop250(@Query("start") int start, @Query("count") int count);

    //带参数
    @GET("openapi")
    Call<Translation> getCall();

    //不带参数
    @HTTP(method = "GET", path = "openapi", hasBody = false)
    Call<Translation> getCallByHttp();

    //带参数
    @HTTP(method = "GET", path = "openapi/{id}", hasBody = false)
    Call<Translation> getCallByHttpParams(@Path("id") int id);

    //2.请求标志

    /**
     * 表明是一个表单格式的请求（Content-Type:application/x-www-form-urlencoded）
     * Field("username") 表示将后面的 String name中name的取值作为 username 的值
     */
    @POST("/form")
    @FormUrlEncoded
    Call<Translation> testFormUrlEncoded1(@Field("username") String name, @Field("age") int age);

    /**
     * @Part 后面支持三种类型，RequestBody、MultipartBody.Part 、任意类型
     * 除MultipartBody.Part} 以外，其它类型都必须带上表单字段(MultipartBody.Part} 中已经包含了表单字段的信息)，
     */
    @POST("/form")
    @Multipart
    Call<Translation> testFileUpload1(@Part("name") RequestBody name, @Part("age") RequestBody age, @Part MultipartBody.Part file);

    @POST("translate?doctype=json&jsonversion=&type=&keyfrom=&model=&mid=&imei=&vendor=&screen=&ssid=&network=&abtest=")
    @FormUrlEncoded
    Call<Translation1> getCall(@Field("i") String targetSentence);
}
