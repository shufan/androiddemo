package com.example.heshufan.template.util;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;

/**
 * @author heshufan
 * @date 2019/5/6
 */

public class ViewUtils {

    public static int[] getLocation(View v){
        int[] location = new int[2];
        v.getLocationInWindow(location);
        return location;
    }

    /**
     * 获取状态栏的高度
     * @param context
     * @return
     */
    public static int getStatusBarHeight(Context context){
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen","android");
        int result = resources.getDimensionPixelSize(resourceId);
        return result;
    }
}
