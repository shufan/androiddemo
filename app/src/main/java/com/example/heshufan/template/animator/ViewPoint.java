package com.example.heshufan.template.animator;

/**
 * @author heshufan
 * @date 2019/5/7
 */

import android.graphics.PointF;

/**
 * 保存view四角坐标的类
 */
public class ViewPoint {
    public float translateX;
    public float translateY;
    public PointF sizePoint;

    public ViewPoint() {
        sizePoint = new PointF();
    }

    public ViewPoint(float translateX, float translateY, PointF sizePoint) {
        this.translateX = translateX;
        this.translateY = translateY;
        this.sizePoint = sizePoint;
    }
}
