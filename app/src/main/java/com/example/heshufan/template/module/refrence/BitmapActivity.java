package com.example.heshufan.template.module.refrence;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.LruCache;
import android.widget.ImageView;

import com.example.common.util.CacheUtil;
import com.example.common.util.DensityUtils;
import com.example.common.util.LogUtil;
import com.example.heshufan.template.R;

import butterknife.ButterKnife;

/**
 * @author heshufan
 *         自定义View
 */
public class BitmapActivity extends BaseActivity {
    private ImageView imageView;
    private ImageView imageLruCache;
    private LruCache<String, Bitmap> lruCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitmap);
        ButterKnife.bind(this);
        init();


        //直接加载数据
        //Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.testbitmap);
        //imageView.setImageBitmap(bitmap);

        //压缩后显示
        imageView.setImageBitmap(CacheUtil.decodeSampledBitmapFromResource(getResources(), R.drawable.testbitmap,
                DensityUtils.dp2px(this, 200), DensityUtils.dp2px(this, 300)));

        loadBitmap(R.drawable.testbitmap, imageLruCache);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    LogUtil.d("kk:"+CacheUtil.getBitmapFormCache(lruCache,String.valueOf(R.drawable.testbitmap)));
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void init() {
        imageView = findViewById(R.id.bitmapIV);
        imageLruCache = findViewById(R.id.bitmspLurCache);

        //获取程序可以得到的最大的内存
        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        //缓存图片内存的大小
        int cache = maxMemory / 8;
        lruCache = new LruCache<String, Bitmap>(cache) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                //重写此方法来衡量每张图片的大小，默认返回图片数量
                return value.getByteCount() / 1024;
            }
        };
    }

    private void loadBitmap(int resID, ImageView imageView) {
        final String imageKey = String.valueOf(resID);
        final Bitmap bitmap = CacheUtil.getBitmapFormCache(lruCache,imageKey);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
            LogUtil.d("缓存内存中存在图片" + imageKey);
        } else {
            LogUtil.d("缓存内存中不存在图片" + imageKey);
            imageView.setImageResource(R.drawable.image_place_holder);
            //开启新线程去加载图片到内存
            BitmapWorkTask task = new BitmapWorkTask();
            task.execute(resID);
        }
    }

    /**
     * 新开线程去加载图片
     */
    public class BitmapWorkTask extends AsyncTask<Integer, Void, Bitmap> {

        /**
         * 在主线程中执行，任务开启前的准备工作
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * 开启子线程执行后台任务
         * @param integers 调用execute时传入的参数
         * @return 返回图片
         */
        @Override
        protected Bitmap doInBackground(Integer... integers) {
            //重新获取图片
            Bitmap bitmap = CacheUtil.decodeSampledBitmapFromResource(getResources(),
                    integers[0], 200, 300);
            //将图片加载到缓存内存
            CacheUtil.addBitmapToMemoryCache(lruCache,String.valueOf(integers[0]), bitmap);
            return bitmap;
        }

        /**
         * 在主线程中执行，更新UI进度
         * @param values
         */
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        /**
         * 在主线程中执行，异步任务执行完成后执行，它的参数是doInbackground()的返回值
         * @param bitmap:doInBackground方法返回的
         */
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageView.setImageBitmap(bitmap);
        }
    }
}
