package com.example.heshufan.template.module.refrence;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;

import com.example.common.util.LogUtil;
import com.example.heshufan.template.R;
import com.example.heshufan.template.aidl.Book;
import com.example.heshufan.template.aidl.BookArrivedLisener;
import com.example.heshufan.template.aidl.IBookManager;
import com.example.heshufan.template.service.BookManagerService;

import java.util.List;

import butterknife.ButterKnife;

/**
 * 利用AIDL进行跨进程通信
 *
 * @author heshufan
 */
public class AIDLActivity extends BaseActivity {
    private static final String TAG = "AIDLActivity：";
    private static final int MSG_RECEIVED_BOOK = 0;
    private IBookManager mRemoteManager;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {


             mRemoteManager = IBookManager.Stub.asInterface(service);
            try {
                List<Book> books = mRemoteManager.getBookList();
                LogUtil.d(TAG + books.toString());

                Book book = new Book(3, "Vue");
                mRemoteManager.addBook(book);
                mRemoteManager.setRegisterBookArrivedlistener(lisener);

            } catch (RemoteException e) {
                e.printStackTrace();
            }


        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    private BookArrivedLisener lisener = new BookArrivedLisener.Stub() {
        @Override
        public void onBookArrived(Book newbook) throws RemoteException {
            //这是在Binder线程，需要抛到UI线程去
            mHandler.obtainMessage(MSG_RECEIVED_BOOK,newbook).sendToTarget();
        }
    };

    private static Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_RECEIVED_BOOK:
                    LogUtil.d(TAG + "A new book has arrived");
                    LogUtil.d(TAG + ((Book)msg.obj).getBookName());
                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);
        ButterKnife.bind(this);

        Intent seriviceInt = new Intent(this, BookManagerService.class);
        bindService(seriviceInt, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {

        if (mRemoteManager != null && mRemoteManager.asBinder().isBinderAlive()) {
            try {
                //会失效，listner通过BInder传递后，服务端会重新构建一个listener对象，与客户端的不是
                mRemoteManager.unRegisterBookArrivedlistener(lisener);
                LogUtil.d(TAG + "unRegister");
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        unbindService(connection);
        super.onDestroy();
    }

}
