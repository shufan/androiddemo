package com.example.heshufan.template.binderpool;

import android.os.RemoteException;

import com.example.common.util.LogUtil;
import com.example.heshufan.template.aidl.ICompute;

/**
 * AIDL接口的实现，也就是运行在服务端的方法
 *
 * @author heshufan
 * @date 2019/4/22
 */

public class ComputeImp extends ICompute.Stub {

    @Override
    public int add(int a, int b) throws RemoteException {
        LogUtil.d(a + b);
        return a + b;
    }
}
