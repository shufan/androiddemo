package com.example.heshufan.template.aidl;

import com.example.heshufan.template.aidl.Book;


interface BookArrivedLisener {
    void onBookArrived(in Book newbook);
}
