package com.example.heshufan.template.aidl;

//aidl用到了自定义的parcelable对象，必须新建一个同名的aidl文件，比如用到的com.example.heshufan.template.aidl.Book
import com.example.heshufan.template.aidl.Book;
import com.example.heshufan.template.aidl.BookArrivedLisener;

interface IBookManager {
     List<Book> getBookList();
     //注意这里是in,不是int
     void addBook(in Book book);
     void setRegisterBookArrivedlistener(BookArrivedLisener listner );
     void unRegisterBookArrivedlistener(BookArrivedLisener listner );
}