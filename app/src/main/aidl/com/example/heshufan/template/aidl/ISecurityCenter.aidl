// ISecurityCenter.aidl
package com.example.heshufan.template.aidl;

//加密的AIDL

interface ISecurityCenter {
    String encrypt(String content);
    String decrypt(String password);
}
