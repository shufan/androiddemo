package com.example.apt_annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)//编译时注解
@Target(ElementType.FIELD)//注解范围为类的成员
public @interface MyBindView {
    int value();
}
