package com.example.common.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author heshufan
 * @date 2019/2/18
 */

public class EchoServer {
    private final ServerSocket mServerSocket;

    public EchoServer(int port) throws IOException {
        mServerSocket = new ServerSocket(port);
    }

    public void run() throws IOException {
        Socket client = mServerSocket.accept();
        handleSocket(client);
    }

    //处理客户端的输入
    private void handleSocket(Socket socket) throws IOException {
        InputStream inputStream = socket.getInputStream();
        OutputStream outputStream = socket.getOutputStream();
        byte[] bytes = new byte[1024];
        int n = 0;
        while ((n = inputStream.read(bytes)) > 0) {
            outputStream.write(bytes,0,n);
        }
    }


    public static void main(String[] args) {
        try {
            EchoServer server = new EchoServer(9877);
            server.run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
