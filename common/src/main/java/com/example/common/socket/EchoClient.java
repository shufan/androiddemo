package com.example.common.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @author heshufan
 * @date 2019/2/18
 */

public class EchoClient {
    private final Socket client;

    public EchoClient(String host, int port) throws IOException {
        client = new Socket(host, port);
    }

    public void run() throws IOException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                readResponce();
            }
        });

        //读取客户端的输入，传递给服务器
        OutputStream stream = client.getOutputStream();
        byte[] bytes = new byte[1024];
        int n = 0;
        while ((n = System.in.read(bytes)) > 0) {
            stream.write(bytes, 0, n);
        }

    }

    /**
     * 开辟新线程读取服务器返回的消息
     */
    private void readResponce() {
        try {
            InputStream inputStream = client.getInputStream();
            byte[] bytes = new byte[1024];
            int n = 0;
            while ((n = inputStream.read(bytes)) > 0) {
                System.out.write(bytes, 0, n);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        try {
            EchoClient client = new EchoClient("loaclhost", 9877);
            client.run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
