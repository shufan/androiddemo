package com.example.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.common.R;


public class TopView extends RelativeLayout {
    private Button leftButton, rightButton;
    private TextView tvTitle;

    private int leftTextColor;
    private Drawable leftBackground;
    private String leftText;
    private LayoutParams leftParams;//定义宽高属性

    private int rightTextColor;
    private Drawable rightBackground;
    private String rightText;
    private LayoutParams rightParams;

    private float titleTextSize;
    private int titleTextColor;
    private String title;
    private LayoutParams titleParams;

    public TopView(Context context) {
        super(context);

    }

    public TopView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //从xml中获取我们定义好的属性
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.Topbar);
        //TypedArray中是以键值对形式存储属性，键由AS按照规则命名的，“styleable名_属性名”
        leftTextColor = ta.getColor(R.styleable.Topbar_leftTextColor, 0);
        leftBackground = ta.getDrawable(R.styleable.Topbar_leftBackground);
        leftText = ta.getString(R.styleable.Topbar_leftText);

        rightTextColor = ta.getColor(R.styleable.Topbar_rightTextColor, 0);
        rightBackground = ta.getDrawable(R.styleable.Topbar_rightBackground);
        rightText = ta.getString(R.styleable.Topbar_rightText);

        title = ta.getString(R.styleable.Topbar_mTitle);
        titleTextSize = ta.getDimension(R.styleable.Topbar_mTitleTextSize, 0);
        titleTextColor = ta.getColor(R.styleable.Topbar_mTitleTextColor, 0);

        ta.recycle();//用完记得回收，避免内存浪费，或者缓存引起其他错误

        final int TAG_LEFT = 0x001;
        final int TAG_RIGHT = 0x002;

        //动态生成控件
        leftButton = new Button(context);
        rightButton = new Button(context);
        tvTitle = new TextView(context);
        //给控件设置属性
        leftButton.setBackground(leftBackground);
        leftButton.setText(leftText);
        leftButton.setTextColor(leftTextColor);
        leftButton.setTag(TAG_LEFT);//为了绑定监听时区分，设置一个tag

        rightButton.setBackground(rightBackground);
        rightButton.setText(rightText);
        rightButton.setTextColor(rightTextColor);
        rightButton.setTag(TAG_RIGHT);

        tvTitle.setText(title);
        tvTitle.setTextColor(titleTextColor);
        tvTitle.setTextSize(titleTextSize);
        tvTitle.setGravity(Gravity.CENTER);//设置居中
        //给Topbar设置背景颜色
        setBackgroundColor(0xFFF59563);
        //设置宽高及一些规则属性
        leftParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        leftParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, TRUE);//添加规则左对齐父控件，注意第二个参数TRUE全大写
        addView(leftButton, leftParams);//把控件添加到布局中

        rightParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        rightParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, TRUE);
        addView(rightButton, rightParams);

        titleParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);//高度填充父控件
        titleParams.addRule(RelativeLayout.CENTER_IN_PARENT, TRUE);
        addView(tvTitle, titleParams);
        //4.绑定监听，调接口方法
        OnClickListener sysListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                switch ((int) v.getTag()) {
                    case TAG_LEFT:
                        listener.leftClick();
                        break;
                    case TAG_RIGHT:
                        listener.rightClick();
                        break;
                }
            }
        };
        leftButton.setOnClickListener(sysListener);
        rightButton.setOnClickListener(sysListener);
    }

    //1.定义接口和抽象方法
    public interface TopbarClickListener {
        //左侧点击
        void leftClick();

        //右侧点击
        void rightClick();
    }

    //2.声明接口对象
    private TopbarClickListener listener;

    //3.定义对外监听方法传参接口对象
    public void setTopbarClickListener(TopbarClickListener listener) {
        this.listener = listener;
    }

    //设置左侧button是否可见，同样可以设置右侧等，此处仅拿左侧button来示例
    public void setLeftVisinility(boolean flag) {
        if (flag) leftButton.setVisibility(VISIBLE);
        else leftButton.setVisibility(INVISIBLE);
    }

    public TopView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
