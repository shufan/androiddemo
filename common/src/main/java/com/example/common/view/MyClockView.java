package com.example.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.example.common.R;
import com.example.common.util.DensityUtils;
import com.example.common.util.LogUtil;

import java.util.Calendar;

/**
 * 仿小米时钟
 * @author heshufan
 * @date 2019/1/30
 */

public class MyClockView extends View {
    //画布
    private Canvas mCanvas;
    private Paint mTextPaint;
    private Rect mTextRect;
    private int mDefaultPadding = 40;
    //外围圆圈
    private Paint mCirclePaint;
    private RectF mCircleRectF;
    private int mCircleStrokeWidth = 4;

    //小时圆圈画笔
    private Paint mHourCiclePaint;
    private int mHourCicleWidth = 3;
    //分针圆圈画笔
    private Paint mMinuteCiclePaint;
    private int mMinuteCicleWidth = 8;
    private int mCiclePointerRadius = 15;
    // 秒针画笔
    private Paint mSecondHandPaint;
    //分针画笔
    private Paint mMinuteHandPaint;
    //时针画笔
    private Paint mHourHandPaint;

    // 时针角度
    private float mHourDegree;
    // 分针角度
    private float mMinuteDegree;
    // 秒针角度
    private float mSecondDegree;

    // 时针路径
    private Path mHourHandPath = new Path();
    // 分针路径
    private Path mMinuteHandPath = new Path();
    // 秒针路径
    private Path mSecondHandPath = new Path();

    /* 时钟半径，不包括padding值 */
    private float mRadius = 40f;

    //刻度长度
    private int mScaleLength = 40;
    private int mScaleWidth = 4;
    private Paint mScaleLinePaint;

    //渐变颜色的圆弧
    private RectF mScaleRectF = new RectF();
    // 渐变矩阵，作用在SweepGradient
    private Matrix mGradientMatrix;
    // 梯度扫描渐变
    private SweepGradient mSweepGradient;
    private Paint mScaleArcPaint;

    public MyClockView(Context context) {
        super(context);
    }

    public MyClockView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        //文字12、6、9、3
        mTextRect = new Rect();
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(Color.parseColor("#000000"));
        mTextPaint.setTextSize(DensityUtils.sp2px(context, 14));

        //最外围圆圈
        mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCirclePaint.setColor(Color.parseColor("#000000"));
        mCirclePaint.setStyle(Paint.Style.STROKE);
        mCirclePaint.setStrokeWidth(mCircleStrokeWidth);
        mCircleRectF = new RectF();

        //指针圆
        mHourCiclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mMinuteCiclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        //刻度
        mScaleLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mScaleLinePaint.setStyle(Paint.Style.STROKE);
        mScaleLinePaint.setStrokeWidth(mScaleWidth);
        mScaleLinePaint.setColor(Color.parseColor("#fa9f27"));

        //渐变
        mScaleArcPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mScaleArcPaint.setStrokeWidth(mScaleLength);
        mScaleArcPaint.setStyle(Paint.Style.STROKE);
        mGradientMatrix = new Matrix();
        mSweepGradient = new SweepGradient(300, 300, new int[]{Color.BLACK, Color.WHITE}, new float[]{0.75f, 1f});

        //秒针
        mSecondHandPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        mHourHandPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mHourHandPaint.setStyle(Paint.Style.FILL);
        mHourHandPaint.setColor(Color.parseColor("#000000"));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(setMeasure(widthMeasureSpec), setMeasure(widthMeasureSpec));
    }

    private int setMeasure(int spec) {
        int defaultSize = 800;
        int mode = MeasureSpec.getMode(spec);
        int size = MeasureSpec.getSize(spec);

        if (mode == MeasureSpec.EXACTLY) {
            return size;
        } else if (mode == MeasureSpec.AT_MOST) {
            return Math.min(defaultSize, size);
        } else if (mode == MeasureSpec.UNSPECIFIED) {
            return defaultSize;
        } else {
            return defaultSize;
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        mCanvas = canvas;
        getCurrentTime();
        drawOutSideArc();
        drawScaleLine();
        drawMinutePointer();
        drawHourPointer();
        drawSecondNeedle();

        //重绘，指针动起来
        invalidate();

    }


    /**
     * 秒针
     */
    private void drawSecondNeedle() {
        mCanvas.save();
        mCanvas.rotate(mSecondDegree, getWidth() / 2, getHeight() / 2);
        mSecondHandPath.reset();
        float offset = mDefaultPadding * 2 + mTextRect.height() + mScaleLength + 10;

        //绘制三角形
        mSecondHandPath.moveTo(getWidth() / 2, offset);
        mSecondHandPath.lineTo(getWidth() / 2 - mRadius / 2, offset + mRadius);
        mSecondHandPath.lineTo(getWidth() / 2 + mRadius / 2, offset + mRadius);
        mSecondHandPath.close();
        mSecondHandPaint.setColor(Color.parseColor("#ffffff"));
        mCanvas.drawPath(mSecondHandPath, mSecondHandPaint);
        mCanvas.restore();
    }

    /**
     * 绘制时针
     */
    private void drawHourPointer() {
        mCanvas.save();
        mCanvas.rotate(mHourDegree, getWidth() / 2, getHeight() / 2);
        mHourHandPath.reset();

        //画指针
        mHourHandPath.moveTo(getWidth() / 2 - mHourCicleWidth * 3, getHeight() / 2 - mCiclePointerRadius);
        mHourHandPath.lineTo(getWidth() / 2 - mHourCicleWidth * 3 + mHourCicleWidth,
                mDefaultPadding * 5 + mTextRect.height() + mScaleLength / 2);
        mHourHandPath.quadTo(getWidth() / 2, mDefaultPadding * 4 + mTextRect.height() + mScaleLength / 2,
                getWidth() / 2 + mHourCicleWidth * 3 - mHourCicleWidth, mDefaultPadding * 5 + mTextRect.height() + mScaleLength / 2);
        mHourHandPath.lineTo(getWidth() / 2 + mHourCicleWidth * 3, getHeight() / 2 - mCiclePointerRadius);
        mHourHandPath.close();

        mHourHandPaint.setStyle(Paint.Style.FILL);
        mCanvas.drawPath(mHourHandPath, mHourHandPaint);


        //画指针底部的圆
        mHourCiclePaint.setColor(Color.parseColor("#000000"));
        mHourCiclePaint.setStrokeWidth(mHourCicleWidth);
        mHourCiclePaint.setStyle(Paint.Style.STROKE);
        mCircleRectF.set(getWidth() / 2 - mCiclePointerRadius, getHeight() / 2 - mCiclePointerRadius, getWidth() / 2 + mCiclePointerRadius, getHeight() / 2 + mCiclePointerRadius);
        mCanvas.drawArc(mCircleRectF, 0, 360, false, mHourCiclePaint);
        mCanvas.restore();
    }

    /**
     * 绘制分针
     */
    private void drawMinutePointer() {
        mCanvas.save();
        mCanvas.rotate(mMinuteDegree, getWidth() / 2, getHeight() / 2);
        mMinuteHandPath.reset();

        //画指针
        float offset = mDefaultPadding * 2 + mTextRect.height() + mScaleLength + 15  + mRadius;
        mMinuteHandPath.moveTo(getWidth() / 2 - mHourCicleWidth * 2, getHeight() / 2 - mCiclePointerRadius);
        mMinuteHandPath.lineTo(getWidth() / 2 - mHourCicleWidth * 2 + mMinuteCicleWidth / 2,offset);
        mMinuteHandPath.quadTo(getWidth() / 2, offset-5,
                getWidth() / 2 + mHourCicleWidth * 2 - mHourCicleWidth / 2, offset);
        mMinuteHandPath.lineTo(getWidth() / 2 + mHourCicleWidth * 2, getHeight() / 2 - mCiclePointerRadius);
        mMinuteHandPath.close();

        mMinuteCiclePaint.setStyle(Paint.Style.FILL);
        mCanvas.drawPath(mMinuteHandPath, mMinuteCiclePaint);


        //画指针底部的圆
        mMinuteCiclePaint.setColor(Color.parseColor("#ffffff"));
        mMinuteCiclePaint.setStrokeWidth(mMinuteCicleWidth);
        mMinuteCiclePaint.setStyle(Paint.Style.STROKE);

        mCircleRectF.set(getWidth() / 2 - mCiclePointerRadius, getHeight() / 2 - mCiclePointerRadius, getWidth() / 2 + mCiclePointerRadius, getHeight() / 2 + mCiclePointerRadius);
        mCanvas.drawArc(mCircleRectF, 0, 360, false, mMinuteCiclePaint);
        mCanvas.restore();

    }

    /**
     * 绘制刻度线
     */
    private void drawScaleLine() {
        mCanvas.save();

        //渐变圆弧的矩形
        mScaleRectF.set(mDefaultPadding * 2 + mTextRect.height() + mScaleLength / 2,
                mDefaultPadding * 2 + mTextRect.height() + mScaleLength / 2,
                getWidth() - mDefaultPadding * 2 - mTextRect.height() - mScaleLength / 2,
                getHeight() - mDefaultPadding * 2 - mTextRect.height() - mScaleLength / 2);

        mSweepGradient = new SweepGradient(getWidth() / 2, getHeight() / 2, new int[]{Color.BLACK, Color.WHITE}, new float[]{0.75f, 1f});
        //matrix默认会在三点钟方向开始颜色的渐变，为了吻合钟表十二点钟顺时针旋转的方向，把秒针旋转的角度减去90度
        mGradientMatrix.setRotate(mSecondDegree - 90, getWidth() / 2, getHeight() / 2);
        mSweepGradient.setLocalMatrix(mGradientMatrix);
        mScaleArcPaint.setShader(mSweepGradient);
        mCanvas.drawArc(mScaleRectF, 0, 360, false, mScaleArcPaint);

        //画背景色刻度线
        for (int i = 0; i < 200; i++) {
            mCanvas.drawLine(getWidth() / 2, mDefaultPadding + mScaleLength + mTextRect.height(),
                    getWidth() / 2, mDefaultPadding + 2 * mScaleLength + mTextRect.height(), mScaleLinePaint);
            //canvas旋转 200个刻度线，每次1.8度
            mCanvas.rotate(1.8f, getWidth() / 2, getHeight() / 2);
        }
        mCanvas.restore();
    }

    /**
     * 绘制最外围圆圈
     */
    private void drawOutSideArc() {
        mCanvas.save();
        String[] timeList = new String[]{"12", "3", "6", "9"};
        //得到文本的边界，上下左右，提取到mTextRect中
        mTextPaint.getTextBounds(timeList[0], 0, timeList[0].length(), mTextRect);
        mTextPaint.setTextAlign(Paint.Align.CENTER);

        mCanvas.drawText(timeList[0], getWidth() / 2, mTextRect.height() + mDefaultPadding, mTextPaint);
        mCanvas.drawText(timeList[1], getWidth() - mTextRect.width() / 2 - mDefaultPadding, getHeight() / 2 + mTextRect.height() / 2, mTextPaint);
        mCanvas.drawText(timeList[2], getWidth() / 2, getHeight() - mDefaultPadding, mTextPaint);
        mCanvas.drawText(timeList[3], mTextRect.width() / 2 + mDefaultPadding, getHeight() / 2 + mTextRect.height() / 2, mTextPaint);

        mCircleRectF.set(mDefaultPadding + mTextRect.width() / 2,
                mDefaultPadding + mTextRect.height() / 2,
                getWidth() - mDefaultPadding - mTextRect.width() / 2,
                getHeight() - mDefaultPadding - mTextRect.height() / 2);

        for (int i = 0; i < 4; i++) {
            //ARC：弧 这里是画弧
            mCanvas.drawArc(mCircleRectF, 5 + 90 * i, 80, false, mCirclePaint);
        }
        mCanvas.restore();
    }

    /**
     * 获取当前时间与各个指针的角度
     */
    private void getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        float milliSecond = calendar.get(Calendar.MILLISECOND);
        float second = calendar.get(Calendar.SECOND) + milliSecond / 1000;
        float minute = calendar.get(Calendar.MINUTE) + second / 60;
        float hour = calendar.get(Calendar.HOUR) + minute / 60;
        mSecondDegree = second / 60 * 360;
        mMinuteDegree = minute / 60 * 360;
        mHourDegree = hour / 12 * 360;
    }
}
