package com.example.common.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.LruCache;
import android.widget.ImageView;

import java.io.File;

/**
 * @author heshufan
 * @date 2019/2/22
 */

public class CacheUtil {

    /**
     * 从内存缓存中获取图片
     *
     * @param key 图片唯一标志
     * @return
     */
    public static Bitmap getBitmapFormCache(LruCache<String, Bitmap> lruCache, String key) {
        LogUtil.d("从缓存内存获得图片");
        return lruCache.get(key);
    }

    /**
     * 将bitmap加入到内存缓存
     *
     * @param key    图片唯一标志
     * @param bitmap 图片
     */
    public static void addBitmapToMemoryCache(LruCache<String, Bitmap> lruCache, String key, Bitmap bitmap) {
        if (getBitmapFormCache(lruCache, key) == null) {
            LogUtil.d("存入缓存内存");
            lruCache.put(key, bitmap);
        }
    }

    /**
     * 计算图片压缩的比例,比例必须是2的幂次
     *
     * @param options   可以得到源图片宽和高的option参数
     * @param reqWidth  需要的宽度
     * @param reqHeight 需要的高度
     * @return 压缩的比例
     */
    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // 源图片的高度和宽度
        final int height = options.outHeight;
        final int width = options.outWidth;
        LogUtil.d("height:" + height);
        LogUtil.d("width:" + width);

        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            // 计算出实际宽高和目标宽高的比率
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            // 选择宽和高中最小的比率作为inSampleSize的值，这样可以保证最终图片的宽和高
            // 一定都会大于等于目标的宽和高。
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        LogUtil.d("inSampleSize:" + inSampleSize);
        return inSampleSize;
    }


    /**
     * 获得最佳显示的bitmap
     *
     * @param res       reource
     * @param resId     资源id
     * @param reqWidth  显示控件宽度
     * @param reqHeight 显示控件的高度
     * @return
     */
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {
        // 第一次解析将inJustDecodeBounds设置为true，来获取图片大小
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        // 调用上面定义的方法计算inSampleSize值
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // 使用获取到的inSampleSize值再次解析图片
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /**
     * 根据传入的uniqueName获取硬盘缓存的路径地址。
     */
    public static File getDiskCacheDir(Context context, String uniqueName) {
        String cachePath;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable()) {
            cachePath = context.getExternalCacheDir().getPath();
        } else {
            cachePath = context.getCacheDir().getPath();
        }
        return new File(cachePath + File.separator + uniqueName);
    }
}
