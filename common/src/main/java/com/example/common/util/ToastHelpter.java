package com.example.common.util;

import android.content.Context;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.common.R;

public class ToastHelpter extends Toast {
    private static ToastHelpter toast;

    public ToastHelpter(Context context) {
        super(context);
    }

    /**
     * 显示一个纯文本吐司
     *
     * @param context 上下文
     * @param text    显示的文本
     */
    public static void showText(Context context, CharSequence text) {
        showToast(context, text);
    }

    /**
     * 显示Toast
     *
     * @param context 上下文
     * @param text    显示的文本
     */
    private static void showToast(Context context, CharSequence text) {
        // 初始化一个新的Toast对象
        initToast(context, text);
        toast.setDuration(Toast.LENGTH_SHORT);
        // 显示Toast
        toast.show();
    }

    /**
     * 初始化Toast
     *
     * @param context 上下文
     * @param text    显示的文本
     */
    private static void initToast(Context context, CharSequence text) {
        try {
            cancelToast();
            toast = new ToastHelpter(context);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.toast_layout, null);
            layout.getBackground().setAlpha(180);
            TextView toastText = layout.findViewById(R.id.toast_message);
            toastText.setText(text);
            toast.setView(layout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 显示toast
     */
    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
        }
    }

    /**
     * 隐藏当前Toast
     */
    public static void cancelToast() {
        if (toast != null) {
            toast.cancel();
        }
    }
    /**
     * 当前Toast消失
     */
    @Override
    public void cancel() {
        try {
            super.cancel();
        } catch (Exception e) {

        }
    }

    /**
     * 显示一个富文本吐司
     *
     * @param context 上下文
     * @param text    显示的文本
     */
    public static void showSpannableText(Context context, CharSequence text) {
        showToast(context, text);
    }

}