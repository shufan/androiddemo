package com.example.common.util;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.common.app.Template;

/**
 * Created by 舍长 on 2019/1/8
 * describe:
 */
public class GlideUtil {
    //获取Url地址
    public static void getUrl(String url, ImageView imageView) {
        Glide.with(Template.getApplication()).load(url).into(imageView);
    }
}
