package com.example.common.util;

import android.util.Log;

/**
 * Created by 舍长 on 2018/12/14
 * describe: Log日志工具类
 */
public class LogUtil {
    //普通版
    public static void d(String msg) {
        Log.d("yishanxiansheng", msg);
    }

    //进行类型判断
    public static void d(Object msg) {
        String string = msg.toString();
        Log.d("yishanxiansheng", string);
    }

    //进行类型判断
    public static void ds(String msg1,String msg2) {
        String string1 = msg1.toString();
        String string2 = msg2.toString();
        Log.d("yishanxiansheng", string1+"--"+string2);
    }
}
