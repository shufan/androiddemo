package com.example.common.app;

import android.app.Application;
import android.content.Context;

import com.squareup.leakcanary.LeakCanary;

import java.util.HashMap;

/**
 * 全局配置类
 *
 * @author heshufan
 * @date 2019/3/29
 */

public class Configutor {
    private HashMap<Object, Object> configs = new HashMap();

    private static final class Holder {
        private static final Configutor INSTANCE = new Configutor();
    }

    /**
     * 静态内部类的单例模式
     */
    public static Configutor getInstance()
    {
        return Holder.INSTANCE;
    }

    /**
     * 获取配置HashMap
     *
     * @return
     */
    public HashMap getConfigs() {
        return configs;
    }
}
