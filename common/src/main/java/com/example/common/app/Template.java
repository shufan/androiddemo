package com.example.common.app;

import android.app.Application;
import android.content.Context;

import com.squareup.leakcanary.LeakCanary;

/**
 * Configutor的代理类
 *
 * @author heshufan
 */
public class Template {

    public static Configutor init(Context context){
        Configutor.getInstance().getConfigs().put(ConfigType.APPLICATION_CONTEXT,context);
        return Configutor.getInstance();
    }

    public static Context getApplication(){
        return (Context) Configutor.getInstance().getConfigs().get(ConfigType.APPLICATION_CONTEXT);
    }
}
